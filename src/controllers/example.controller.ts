import {personas, usuarios} from '../mock';
import { Request, Response } from 'express';
// CRUD
// Create - Read - Update - Delete
const getAll = (request: Request, response: Response) => {
	try {
		const { query } = request;
		console.log({query});
		// throw Error('Hay un error');
		return response.send({
			success: true,
			data: personas
		});
	} catch {
		return response.send({
			success: false,
			error: {
				code: '01',
				message: 'Ha ocurrido un error al obtener las personas'
			}
		})
	}
}

const getByName = (request: Request, response: Response) => {
	try {
		const { name } = request.params;
		const personaFinded = personas.filter((persona) => persona.nombre.toLowerCase() === name.toLowerCase());
		if(!personaFinded.length){
			return response.send({
				success: false, 
				error: {code: '02', message: 'Personas no encontradas'}
			});
		}
		return response.send({
			success: true,
			data: personaFinded
		});
	} catch {
		return response.send({
			success: false,
			error: {
				code: '01',
				message: 'Ha ocurrido un error al obtener las personas'
			}
		})
	}
}

const login = (request: Request, response: Response) => {
	try {
		const { username, password } = request.query;
		console.log('login!')
		const user = usuarios.some(usuario => usuario.username === username && usuario.password === password);
		if(!user){
			return response.send({
				success: false,
				error: {
					code: '03',
					message: 'Usuario o password incorrecto'
				}
			});
		}
		return response.send({
			success: true,
			message: 'Usuario logeado correctamente'
		})

	} catch {
		return response.send({
			success: false,
			error: {
				code: '01',
				message: 'Ha ocurrido un error al obtener las personas'
			}
		})
	}
}

const loginPost = (request: Request, response: Response) => {
	try {
		const { username, password } = request.body;
		console.log('login!', request.body)
		const user = usuarios.some(usuario => usuario.username === username && usuario.password === password);
		if(!user){
			return response.send({
				success: false,
				error: {
					code: '03',
					message: 'Usuario o password incorrecto'
				}
			});
		}
		return response.send({
			success: true,
			message: 'Usuario logeado correctamente'
		})

	} catch (err: any) {
		console.error(err);
		return response.send({
			success: false,
			error: {
				code: '01',
				message: 'Ha ocurrido un error al logearse'
			}
		})
	}
}

//TODO - crear una funcion de actualizar personas
const updateUser = (req: Request, res: Response) => {
    try{
        const { id } = req.params;
        const numberId = Number(id);
        if(isNaN(numberId)){
            res.status(400).send({
                success: false,
                message: 'Invalid id'
            })
        }
        const {_id, ...body} = req.body;
        const index = usuarios.findIndex(usuario => usuario._id === numberId )
        if(index === -1){
            res.send({
                success: false,
                message: `Usuario con ID ${numberId} no encontrado`
            })
        }

        usuarios[index] = {
            ...usuarios[index],
            ...body
        }

        res.send({
            success: true,
            message: 'Usuario actualizado',
            data: usuarios[index]
        })

    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}

const deleteUser = (req: Request, res: Response) => {
    try{
        const { id } = req.params;
        const numberId = Number(id);
        if(isNaN(numberId)){
            res.status(400).send({
                success: false,
                message: 'Invalid id'
            })
        }
        const index = usuarios.findIndex(usuario => usuario._id === numberId )
        if(index === -1){
            res.send({
                success: false,
                message: `Usuario con ID ${numberId} no encontrado`
            })
        }

        usuarios.splice(index, 1);

        res.send({
            success: true,
            message: `Usuario con ID ${numberId} eliminado!`
        })
    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}

const listUser = (res: Response) => {
    try {
        res.send({
            success: true,
            message: 'Success',
            data: usuarios
        })
    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}


export {
	getAll,
	getByName,
	login,
	loginPost,
	updateUser,
	deleteUser,
	listUser
}