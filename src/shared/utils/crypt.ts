import AES from 'crypto-js/aes';
import Crypto from 'crypto-js';

//TODO - UTILIZAR OTRO METODO DE ENCRIPTACION

function encrypt(text: string): string {
	const passwordCypher = AES.encrypt(text, process.env.SALT!);
	return passwordCypher.toString();
}

function decrypt(hash: string): any {
	const decrypted = AES.decrypt(hash, process.env.SALT!).toString(Crypto.enc.Utf8);
	return JSON.parse(decrypted);
}

function compareSync(hash: string, compare: string): boolean {
	const decrypted = decrypt(hash);
	console.log({decrypted});
	if(compare === String(decrypted) ){
		return true;
	}
	return false;
}

// METODO DE ENCRIPTACION BASE 64
function encrypt64(text: string): string{
	let encrypt = Buffer.from(text);
	return encrypt.toString("base64");
}

function decrypt64(text: string): string{
	let decrypt = Buffer.from(text, "base64");
	return decrypt.toString();
}

function compare64(hash: string, compare: string): boolean {
	const decrypted = decrypt64(hash);
	return (compare === String(decrypted)) ? true : false;
}

export {
	encrypt,
	decrypt,
	compareSync,
	encrypt64,
	decrypt64,
	compare64
}